<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 07:54 ч.
 */
namespace App\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'ratings';
    protected $fillable = [
        'created_at',
        'updated_at',
        'rating',
        'recipe_id',
    ];
}