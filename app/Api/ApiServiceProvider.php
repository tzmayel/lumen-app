<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 03:13 ч.
 */

namespace App\Api;

use Illuminate\Support\ServiceProvider;
use App\Api\Services\RecipeService;
use App\Api\Repositories\RecipeRepository;


class ApiServiceProvider  extends ServiceProvider
{
    public function boot()
    {
        require __DIR__ . '/../Api/routes.php';
    }

    public function register(){
        $this->app->bind(RecipeService::class, function ($app) {
            return new RecipeService(
                new RecipeRepository()
            );
        });

        $this->app->bind(RatringService::class, function ($app) {
            return new RatringService(
                new RatingRepository(),
                new RecipeService()
            );
        });
    }


}