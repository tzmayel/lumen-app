<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 03:51 ч.
 */

namespace App\Api\Services;

use App\Api\Repositories\RatingRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RatingService
{
    /** @var RatingRepository  */
    private $repository;

    /** @var RecipeService  */
    private $recipeService;

    public function __construct(RatingRepository $repository, RecipeService $recipeService)
    {
        $this->repository = $repository;
        $this->recipeService = $recipeService;
    }

    public function create($rating, $recipe_id)
    {
        if($rating < 1 || $rating > 5) {
            throw new BadRequestHttpException('Not valid rating');
        }
        try{
            $this->recipeService->getById($recipe_id);
            return $this->repository->insert(
                [
                    'recipe_id' => $recipe_id,
                    'rating' => $rating
                ]
            );
        }
        catch (ModelNotFoundException $ex){
            throw new NotFoundHttpException('recipe not found, message: '.$ex->getMessage());
        }
    }
}