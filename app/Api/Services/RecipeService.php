<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 03:51 ч.
 */

namespace App\Api\Services;

use App\Api\Repositories\RecipeRepository;

class RecipeService
{
    /** @var RecipeRepository  */
    private $repository;

    public function __construct(RecipeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        return $this->repository->all();
    }

    public function getById($id){
        return $this->repository->getById($id);
    }

    public function find(array $where = [])
    {
        return $this->repository->find($where);
    }

    public function create(array $data)
    {
        return $this->repository->insert($data);
    }

    public function update($id, array $data){
        return $this->repository->update($id, $data);
    }
}