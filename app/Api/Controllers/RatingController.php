<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 03:38 ч.
 */

namespace App\Api\Controllers;

use App\Api\Services\RatingService;
use App\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;

class RatingController extends Controller
{
    use Helpers;

    /** @var RatingService  */
    private $service;

    public function __construct(RatingService $service)
    {
        $this->service = $service;
    }

    public function create(Request $request, $recipe_id, $rating)
    {
        return $this->response()->array(
            $this->service->create($rating, $recipe_id)
        )->setStatusCode(Response::HTTP_CREATED);
    }

}