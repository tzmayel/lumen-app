<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 03:38 ч.
 */

namespace App\Api\Controllers;

use App\Api\Services\RecipeService;
use App\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;

class RecipeController extends Controller
{
    use Helpers;
    /** @var RecipeService  */
    private $service;


    public function __construct(RecipeService $service)
    {
        $this->service = $service;
    }

    public function list()
    {
        return $this->response()->array(
            $this->service->getAll()
        );
    }

    public function getById(Request $request, $id)
    {
        return $this->response()->array(
            $this->service->getById($id)
        );
    }

    public function getByCuisine(Request $request, $cuisine_type)
    {
        return $this->response()->array(
            $this->service->find(
                array(
                    array(
                        'recipe_cuisine',
                        '=',
                        $cuisine_type
                    )
                )
            )
        );
    }

    public function create(Request $request)
    {
        $inputs = $request->all();
        return $this->response()->array(
            $this->service->create($inputs)
        )->setStatusCode(Response::HTTP_CREATED);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        return $this->response()->array(
            $this->service->update($id, $inputs)
        );
    }
}