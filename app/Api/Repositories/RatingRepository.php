<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 07:50 ч.
 */

namespace App\Api\Repositories;

use Dingo\Api\Routing\Helpers;
use App\Api\Models\Rating;
use DB;

class RatingRepository implements RepositoryBase
{
    use Helpers;

    public function all()
    {
        return Rating::all();
    }

    public function insert(array $data)
    {
        return Rating::create($data);
    }

    public function update($id, array $data)
    {
        $R = Rating::findOrFail($id);
        $R->update($data);
        return $R;
    }

    public function getById($id){
        //we can user find with 'id' '=' $id
        //bit we can use this function with findOrDie
        return Rating::findOrFail($id);
    }

    public function find(array $where){
        return DB::table('ratings')->where($where)->get();
    }
}