<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 07:50 ч.
 */

namespace App\Api\Repositories;

use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use App\Api\Models\Recipe;
use DB;


class RecipeRepository implements RepositoryBase
{
    use Helpers;

    public function __construct()
    {
    }

    public function all()
    {
        return Recipe::all();
    }


    public function insert(array $data)
    {
        return Recipe::create($data);
    }

    public function update($id, array $data)
    {
        $R = Recipe::findOrFail($id);
        $R->update($data);
        return $this->response()->array(
            $R
        );
    }

    public function getById($id){
        //we can user find with 'id' '=' $id
        //bit we can use this function with findOrDie
        return $this->response()->array(
            Recipe::findOrFail($id)
        );
    }

    public function find(array $where){
        return $this->response()->array(
            DB::table('recipes')->where($where)->get()
        );
    }
}