<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 03:58 ч.
 */

namespace App\Api\Repositories;


interface RepositoryBase
{
    public function all();
    public function insert(array $data);
    public function update($id, array $data);
    public function find(array $where);
    public function getById($id);
}