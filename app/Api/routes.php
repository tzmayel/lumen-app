<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 03:19 ч.
 */

$api = app(Dingo\Api\Routing\Router::class);

$api->group(
    [
        'namespace' => 'App\Api\Controllers',
        'version' => 'v1'
    ],
    function ($api) {

        // routes for recipes
        $api->get('recipes', 'RecipeController@list');
        $api->get('recipes/cuisine/{type}', 'RecipeController@getByCuisine');
        $api->post('recipe/add/', 'RecipeController@create');
        $api->put('recipe/{id}/up/', 'RecipeController@update');
        $api->get('recipe/{id}', 'RecipeController@getById');

        // routes for ratings
        $api->post('recipe/{recipe_id}/rate/{rating}', 'RatingController@create');

    }
);