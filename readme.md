# Tech Test 

For the current API test I'm using Laravel + Lumen + Dingo because it's light.

Usage:
Routes for recipes:

GET /recipes - list all recipes

GET /recipes/cuisine/{type} - Fetch all recipes for a specific cuisine

POST /recipe/add/ - Store a new recipe

PUT /recipe/{id}/up/ - Update an existing recipe

GET /recipe/{id} - Fetch a recipe by id


Routes for ratings:
POST /recipe/{recipe_id}/rate/{rating} - Rate an existing recipe between 1 and 5

Unit test:
usage: ./vendor/bin/phpunit

Database: SQLite