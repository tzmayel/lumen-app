<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Dingo\Api\Http\Response;

class ExampleTest extends TestCase
{
    private $fields_names = [
        'id',
        'created_at',
        'updated_at',
        'box_type',
        'title',
        'slug',
        'short_title',
        'marketing_description',
        'calories_kcal',
        'protein_grams',
        'fat_grams',
        'carbs_grams',
        'bulletpoint1',
        'bulletpoint2',
        'bulletpoint3',
        'recipe_diet_type_id',
        'season',
        'base',
        'protein_source',
        'preparation_time_minutes',
        'shelf_life_days',
        'equipment_needed',
        'origin_country',
        'recipe_cuisine',
        'in_your_box',
        'gousto_reference'
    ];


    public function setUp(){
        parent::setUp();
        $this->baseUrl = env('API_SERVER_ADDRESS');
    }

    public function testGetRecepies()
    {
        $this->get('/recipes')->seeJsonStructure([
                'recipes' => [
                    '*' => $this->fields_names
                ]
            ])->assertResponseOk();
    }

    public function testGetRecipeById()
    {
        $this->get('/recipe/4')->seeJsonStructure([
                '*' => $this->fields_names
        ])->assertResponseOk();
    }

    public function testGetByCuisine()
    {
        $this->get('/recipes/cuisine/british')->seeJsonContains([
            'title' =>  'Courgette Pasta Rags'
        ])->assertResponseOk();
    }

    public function testUpdate()
    {
        $this->put('/recipe/3/up/?title=new Title&short_title=delicious')->seeJsonContains([
            'title' =>  'new Title',
            'id' =>  3,
            'short_title' =>  'delicious'
        ])->assertResponseOk();
    }

    public function testInserNewRecipe()
    {
        $this->post('/recipe/add/?title=title for my new recipe&short_title=delicious3')->seeJsonContains([
            'title' =>  'title for my new recipe',
            'short_title' =>  'delicious3'
        ])->assertResponseStatus(Response::HTTP_CREATED);
    }

    public function testGiveRatingToRecipe()
    {
        $this->post('/recipe/4/rate/5')->assertResponseStatus(Response::HTTP_CREATED);
    }

    public function testGiveRatingToInvalidRecipe()
    {
        $this->post('/recipe/-4/rate/5')->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }

    public function  testGiveInvalidRatingRecipe_part1()
    {
        $this->post('/recipe/4/rate/6')->assertResponseStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testGiveInvalidRatingRecipe_part2()
    {
        $this->post('/recipe/4/rate/0')->assertResponseStatus(Response::HTTP_BAD_REQUEST);
    }



}
