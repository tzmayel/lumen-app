<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 25.7.2018 г.
 * Time: 06:02 ч.
 */
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RecipesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('recipes')->delete();
        $file = env('MOCKDATA');
        $handle = fopen("$file", "r");

        if ($handle) {
            $col_names = DB::connection('sqlite')->getSchemaBuilder()->getColumnListing("recipes");
            $keys = [];

            foreach($col_names as $col_name){
                $keys[] = $col_name;
            }

            //remove id
            array_shift($keys);

            $is_first_line = true;

            while (($line = fgets($handle)) !== false) {

                if($is_first_line){
                    //skip first line
                    $is_first_line = false;
                    continue;
                }
                $values = str_getcsv($line);

                //fix datetime format
                $values[1] = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$values[1])));
                $values[2] = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$values[2])));

                //remove id
                array_shift($values);

                if(count($keys)  == count($values)) {
                    $arr = array_combine($keys, $values);
                    DB::table('recipes')->insert($arr);
                }
                else{
                    throw new Exception("not equal ".count($keys)." ".count($values));
                }
            }
            fclose($handle);
        } else {
            throw new Exception("can't open file=".$file);
        }
    }
}