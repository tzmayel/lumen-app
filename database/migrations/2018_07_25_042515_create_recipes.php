<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime("created_at")->nullable();
            $table->dateTime("updated_at")->nullable();
            $table->string("box_type")->nullable();
            $table->string("title", 512)->nullable();
            $table->string("slug", 512)->nullable();
            $table->string("short_title", 256)->nullable();
            $table->mediumText("marketing_description")->nullable();
            $table->integer("calories_kcal")->nullable();
            $table->integer("protein_grams")->nullable();
            $table->integer("fat_grams")->nullable();
            $table->integer("carbs_grams")->nullable();
            $table->string("bulletpoint1", 512)->nullable();
            $table->string("bulletpoint2", 512)->nullable();
            $table->string("bulletpoint3", 512)->nullable();
            $table->integer("recipe_diet_type_id")->nullable();
            $table->string("season", 32)->nullable();
            $table->string("base", 32)->nullable();
            $table->string("protein_source", 256)->nullable();
            $table->integer("preparation_time_minutes")->nullable();
            $table->decimal("shelf_life_days")->nullable();
            $table->mediumText("equipment_needed")->nullable();
            $table->string("origin_country", 256)->nullable();
            $table->string("recipe_cuisine", 256)->nullable();
            $table->string("in_your_box", 256)->nullable();
            $table->string("gousto_reference", 256)->nullable();
        });


        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id');
            $table->integer('rating');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
        Schema::dropIfExists('ratings');
    }
}
